#include "util.h"
#include <stdlib.h>


FILE * open_file(const char * filename, const char * options) {
    FILE * file;
    file = fopen(filename, options);
    if (!file) {
        exit(-1);
    }
    return file;
}

void close_file(FILE * file){
    fclose(file);
    if (file) {
        exit(-1);
    }
}
