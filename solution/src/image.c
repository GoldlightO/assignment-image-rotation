#include "image.h"


struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct pixel { uint8_t b, g, r; };

struct image * create_image(){
    return (struct image *) malloc(sizeof(struct image));
}

struct pixel * create_pixels(uint64_t width, uint64_t height) {
    return (struct pixel *) malloc(width * height * sizeof(struct pixel));
}

void destroy_pixels(struct pixel * pixels){
    free(pixels);
    pixels = NULL;
}

void destroy_image(struct image * img) {
    destroy_pixels(img->data);
    free(img);
    img = NULL;
}

uint64_t get_width(const struct image * img){
    return img->width;
}

uint64_t get_height(const struct image * img){
    return img->height;
}

struct pixel * get_pixel(const struct image * img) {
    return img->data;
}

struct pixel * get_current_pixel(const struct image * img, size_t index) {
    return &(img->data[index]);
}

void set_current_pixel(struct image * img, size_t index, struct pixel * value) {
    img->data[index] = *value;
}

void set_width(struct image * img, uint64_t width_) {
    img->width = width_;
}

void set_height(struct image * img, uint64_t height_) {
    img->height = height_;
}

void set_pixel(struct image * img, uint64_t data_size) {
    img->data = (struct pixel *) malloc(data_size);
}

size_t get_pixel_size(){
    return sizeof(struct pixel);
}

uint64_t get_data_pixel_size(uint64_t width, uint64_t height) {
    return width * height * get_pixel_size();
}

long get_padding(const struct image * img){
    return (long)(img->width % 4);
}

enum read_status read_image(FILE * in , struct image * img){
    if (in != NULL) {
        long padding = get_padding(img);
        for (size_t i = 0; i < img->height; i++) {
            fread(&(img->data[i * img->width]), sizeof(struct pixel), img->width, in);
            fseek(in, padding, SEEK_CUR);
        }
        return READ_OK;
    }
    return READ_ERROR;
}

enum write_status write_image(FILE * out, struct image * img){
    const struct pixel zero[] = {{0},{0},{0}};
    uint64_t wdt = get_width(img);
    long padding = get_padding(img);
    for (size_t i = 0; i < get_height(img); i++) {
        if(!fwrite(&(img->data[i * wdt]), sizeof(struct pixel), wdt, out) ||
                (padding && !fwrite(zero, 1, padding, out))){
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}
