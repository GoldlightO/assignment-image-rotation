#include <stdio.h>

#include "rotate.h"

struct image * rotate(const struct image *  img){
    struct image * new_img = create_image();
    uint64_t prev_width = get_width(img);
    uint64_t prev_height = get_height(img);
    set_height(new_img, prev_width);
    set_width(new_img, prev_height);
    set_pixel(new_img, get_data_pixel_size(prev_width, prev_height));
    for (uint64_t i = 0; i < prev_height; i++) {
        for (uint64_t j = 0; j < prev_width; j++) {
            set_current_pixel(new_img, (get_height(img) - 1 - i) + get_height(img) * j, get_current_pixel(img, get_width(img) * i + j));
 //           set_current_pixel(new_img, i + (get_height(new_img) - j - 1) * get_width(new_img), get_current_pixel(img, get_width(img) * i + j));
        }
    }
    return new_img;
}
