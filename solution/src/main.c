#include <stdio.h>

#include "drawing_bmp.h"
#include "rotate.h"
#include "util.h"



int main( int argc, char** argv ) {
    if (argc != 3){
        printf("Usage: ./image-transformer <source-image> <transformed-image>\n");
        return -1;
    }
    FILE * input_file;
    input_file = open_file(argv[1], "r");
    struct drawing_image * drw_image = create_drawing_image();
    struct bmp_header * header = create_header();
    struct image * img = create_image();
    read_bmp_header(input_file, header);
    set_bmp_header(drw_image,header);
    set_height(img,get_height_bmp(header));
    set_width(img, get_width_bmp(header));
    set_pixel(img, get_data_pixel_size(get_width_bmp(header), get_height_bmp(header)));
    read_image(input_file, img);
    set_image(drw_image, img);
    FILE * output_file;
    output_file = open_file(argv[2], "w");
    struct drawing_image * new_dr = create_drawing_image();
    struct bmp_header * val = generate_header_to_image(header, img);
    struct image * val1 = rotate(img);
    set_bmp_header(new_dr, val);
    set_image(new_dr, val1);
    write_bmp_header(output_file, get_bmp_header(new_dr));
    write_image(output_file, get_image(new_dr));
    destroy_header(header);
    destroy_image(img);
    destroy_header(val);
    destroy_image(val1);
    destroy_drawing_image(drw_image);
    destroy_drawing_image(new_dr);
    return 0;
}


