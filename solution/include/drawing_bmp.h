#ifndef ASSIGNMENT_IMAGE_ROTATION_MASTER_DRAWING_H
#define ASSIGNMENT_IMAGE_ROTATION_MASTER_DRAWING_H

#include "bmp_file.h"
#include "image.h"

struct drawing_image;

struct drawing_image * create_drawing_image();

void destroy_drawing_image(struct drawing_image * draw);

struct bmp_header * get_bmp_header(struct drawing_image * draw);

struct image * get_image(struct drawing_image * draw);

void set_bmp_header(struct drawing_image * draw, struct bmp_header * header);

void set_image(struct drawing_image * draw, struct image * image);

#endif //ASSIGNMENT_IMAGE_ROTATION_MASTER_DRAWING_H
