#ifndef ASSIGNMENT_IMAGE_ROTATION_MASTER_UTIL_H
#define ASSIGNMENT_IMAGE_ROTATION_MASTER_UTIL_H

#include <stdio.h>

FILE * open_file(const char * filename, const char * options);

void close_file(FILE * file);

#endif //ASSIGNMENT_IMAGE_ROTATION_MASTER_UTIL_H
